import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData buildLightTheme = ThemeData();
  static ThemeData buildDarkTheme = ThemeData();
}
