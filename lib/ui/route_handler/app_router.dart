import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:github_stars/core/models/repository_model.dart';
import 'package:github_stars/ui/screens/repository_list/repository_details_page.dart';
import 'package:injectable/injectable.dart';

import 'package:github_stars/ui/screens/repository_list/repository_list_page.dart';

part 'app_router.gr.dart';

@singleton
@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  final List<AutoRoute> routes = [
    AutoRoute(path: '/', page: RepositoryListRoute.page),
    AutoRoute(page: RepositoryDetailsRoute.page),
  ];
}
