import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:github_stars/core/models/repository_model.dart';
import 'package:github_stars/core/utils/common/extensions.dart';
import 'package:github_stars/core/utils/common/filter_type.dart';
import 'package:github_stars/ui/route_handler/app_router.dart';
import 'package:github_stars/ui/widgets/stateful_wrapper.dart';
import 'package:github_stars/ui/widgets/locading_indicator.dart';
import 'package:github_stars/core/utils/injectable/injectable.dart';
import 'package:github_stars/core/blocs/repository_list/repository_list_cubit.dart';

@RoutePage()
class RepositoryListPage extends StatelessWidget {
  const RepositoryListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: getIt<RepositoryListCubit>(),
      child: _RepositoryListBody(),
    );
  }
}

class _RepositoryListBody extends StatelessWidget {
  final _scrollController = ScrollController();

  void _setupScrollController(context) {
    _scrollController.addListener(() {
      if (_scrollController.position.atEdge) {
        if (_scrollController.position.pixels != 0) {
          BlocProvider.of<RepositoryListCubit>(context).getRepositoriesList(isScrolling: true);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return StatefulWrapper(
      onInit: () {
        _setupScrollController(context);
        context.read<RepositoryListCubit>().getRepositoriesList();
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Github Stars'),
          actions: [
            BlocBuilder<RepositoryListCubit, RepositoryListState>(
              builder: (context, state) {
                if (state is RepositoryListLoaded) {
                  return PopupMenuButton<String>(
                    onSelected: (value) {},
                    itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                      PopupMenuItem<String>(
                        value: FilterType.star,
                        child: Row(children: [
                          Checkbox(
                            value: state.filterType == FilterType.star,
                            onChanged: (value) => value,
                          ),
                          const Text('Sort by Star Count')
                        ]),
                        onTap: () => context.read<RepositoryListCubit>().updateFilterType(FilterType.star),
                      ),
                      PopupMenuItem<String>(
                        value: FilterType.dateTime,
                        child: Row(
                          children: [
                            Checkbox(
                              value: state.filterType == FilterType.dateTime,
                              onChanged: (value) => value,
                            ),
                            const Text('Sort by Updated At'),
                          ],
                        ),
                        onTap: () => context.read<RepositoryListCubit>().updateFilterType(FilterType.dateTime),
                      ),
                    ],
                  );
                }
                return const SizedBox();
              },
            ),
          ],
        ),
        body: BlocBuilder<RepositoryListCubit, RepositoryListState>(
          builder: (context, state) {
            if (state is RepositoryListFailedToLoad) {
              return const Center(
                child: Text('Failed to fetch data'),
              );
            }
            if (state is RepositoryListLoading && state.isFirstFetch) {
              return const LoadingIndicator();
            }

            List<RepositoryModel> repos = [];
            bool isLoading = false;

            if (state is RepositoryListLoading) {
              repos = state.oldRepositories;
              isLoading = true;
            } else if (state is RepositoryListLoaded) {
              repos = state.repositories;
            }

            return ListView.separated(
              controller: _scrollController,
              itemCount: repos.length + (isLoading ? 1 : 0),
              separatorBuilder: (context, index) => const Divider(),
              itemBuilder: (context, index) {
                if (index < repos.length) {
                  return _RepositoryListItem(repositoryModel: repos[index]);
                } else {
                  Timer(const Duration(milliseconds: 30), () {
                    _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
                  });

                  return const LoadingIndicator();
                }
              },
            );
          },
        ),
      ),
    );
  }
}

class _RepositoryListItem extends StatelessWidget {
  final RepositoryModel repositoryModel;
  const _RepositoryListItem({required this.repositoryModel});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => context.router.push(RepositoryDetailsRoute(repositoryModel: repositoryModel)),
      leading: CircleAvatar(backgroundImage: NetworkImage(repositoryModel.owner.avatarUrl)),
      title: Text(
        repositoryModel.owner.login,
        style: const TextStyle(fontSize: 16.0),
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            repositoryModel.description ?? 'Description not available',
            style: const TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 4.0),
          Text(
            'Last updated: ${repositoryModel.updatedAt.toFormattedTime}',
            style: const TextStyle(fontSize: 12.0),
          ),
        ],
      ),
    );
  }

  _filterDialog(BuildContext context, String catalogType) async {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Sort by"),
          contentPadding: const EdgeInsets.all(0.0),
          content: SizedBox(
            width: double.maxFinite,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  margin: const EdgeInsets.only(bottom: 10.0),
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      RadioListTile<String?>(
                        visualDensity: const VisualDensity(
                            horizontal: VisualDensity.minimumDensity, vertical: VisualDensity.minimumDensity),
                        value: FilterType.star,
                        title: const Text('Sort by Star Count'),
                        onChanged: (String? type) {
                          if (type != null) context.read<RepositoryListCubit>().updateFilterType(type);
                          context.router.pop();
                        },
                        groupValue: catalogType,
                      ),
                      RadioListTile<String?>(
                        visualDensity: const VisualDensity(
                            horizontal: VisualDensity.minimumDensity, vertical: VisualDensity.minimumDensity),
                        value: FilterType.dateTime,
                        title: const Text('Sort by Updated Time'),
                        onChanged: (String? type) {
                          if (type != null) context.read<RepositoryListCubit>().updateFilterType(type);
                          context.router.pop();
                        },
                        groupValue: catalogType,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
