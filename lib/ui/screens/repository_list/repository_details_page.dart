import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import 'package:github_stars/core/models/repository_model.dart';
import 'package:github_stars/core/utils/common/extensions.dart';

@RoutePage()
class RepositoryDetailsPage extends StatelessWidget {
  final RepositoryModel repositoryModel;
  const RepositoryDetailsPage({super.key, required this.repositoryModel});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Repository Details'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Owner Name: ${repositoryModel.owner.login}',
              style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Image.network(
              repositoryModel.owner.avatarUrl,
              height: 200,
              width: 200,
              fit: BoxFit.cover,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Description: ${repositoryModel.description ?? ''}',
              style: const TextStyle(fontSize: 16),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Last Update: ${repositoryModel.updatedAt.toFormattedTime}',
              style: const TextStyle(fontSize: 16),
            ),
          ),
        ],
      ),
    );
  }
}
