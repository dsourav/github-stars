import 'package:flutter/material.dart';
import 'package:flavor_config/flavor_config.dart';

import 'package:github_stars/github_stars_app.dart';
import 'package:github_stars/core/utils/configs/dev_config.dart';
import 'core/utils/injectable/injectable.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  FlavorConfig(
    flavorName: "DEVELOP",
    values: devConfig,
  );

  await configureDependencies();
  runApp(const GithubStarsApp());
}
