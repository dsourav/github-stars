// TODO Json Serializable needs to be used for generating toMap and fromMap

class RepositoryModel {
  final int id;
  final String? description;
  final String name;
  final String fullName;
  final int stargazersCount;
  final String updatedAt;
  final Owner owner;

  RepositoryModel({
    required this.id,
    this.description,
    required this.stargazersCount,
    required this.name,
    required this.fullName,
    required this.owner,
    required this.updatedAt,
  });

  factory RepositoryModel.fromMap(Map<String, dynamic> map) {
    return RepositoryModel(
      id: map['id'],
      description: map['description'],
      name: map['name'],
      fullName: map['full_name'],
      updatedAt: map['updated_at'],
      stargazersCount: map['stargazers_count'],
      owner: Owner.fromMap(map['owner']),
    );
  }

  factory RepositoryModel.fromLocalDbMap(Map<String, dynamic> map) {
    return RepositoryModel(
        id: map['id'],
        description: map['description'],
        name: map['name'],
        fullName: map['full_name'],
        updatedAt: map['updated_at'],
        stargazersCount: map['stargazers_count'],
        owner: Owner.fromMap(map));
  }

  static Map<String, dynamic> toMap(RepositoryModel repositoryModel) {
    return {
      'id': repositoryModel.id,
      'description': repositoryModel.description,
      'name': repositoryModel.name,
      'full_name': repositoryModel.fullName,
      'updated_at': repositoryModel.updatedAt,
      'stargazers_count': repositoryModel.stargazersCount,
      'login': repositoryModel.owner.login,
      'avatar_url': repositoryModel.owner.avatarUrl,
    };
  }
}

class Owner {
  final String login;
  final String avatarUrl;

  Owner({required this.login, required this.avatarUrl});

  factory Owner.fromMap(Map<String, dynamic> map) {
    return Owner(
      login: map['login'] ?? '',
      avatarUrl: map['avatar_url'] ?? '',
    );
  }
}
