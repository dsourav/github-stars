import 'package:dio/dio.dart';

import 'package:github_stars/core/utils/exception_handler/app_exception.dart';

class DioExceptionHanler extends AppException {
  DioExceptionHanler(DioException error) : super(_errorMessage(error));

  static String _errorMessage(DioException error) {
    return error.message ?? 'Unknown error occured';
  }
}
