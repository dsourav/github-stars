class DbUtils {
  static String dbName = 'github_starred.db';
  static int dbVersion = 1;
  static String repositoriesTable = 'Repositories';
  static String createRepositoriesTableQuery =
      'CREATE TABLE IF NOT EXISTS $repositoriesTable (id INTEGER PRIMARY KEY, name TEXT, full_name TEXT, description TEXT, updated_at TEXT, stargazers_count INTEGER, login TEXT, avatar_url TEXT)';
}
