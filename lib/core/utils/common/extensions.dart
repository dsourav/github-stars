import 'package:intl/intl.dart';

extension StringExtension on String {
  String get toFormattedTime {
    try {
      final DateTime dateTime = DateTime.parse(this);
      final DateFormat formatter = DateFormat('MM-dd-yyyy HH:mm', 'en_US');
      return formatter.format(dateTime);
    } catch (_) {
      return '';
    }
  }

  int get toEpochTime {
    try {
      final DateTime? dateTime = DateTime.tryParse(this);

      return dateTime?.microsecondsSinceEpoch ?? -1;
    } catch (_) {
      return -1;
    }
  }
}
