import 'package:dio/dio.dart';
import 'package:flavor_config/flavor_config.dart';
import 'package:injectable/injectable.dart';

@module
abstract class DioClientModule {
  @singleton
  Dio get dio {
    final dio = Dio();
    dio.options.baseUrl = FlavorConfig.instance.values['base_url'];
    dio.options.connectTimeout = const Duration(seconds: 30);
    dio
      ..interceptors.clear()
      ..interceptors.add(LogInterceptor(responseBody: false));
    return dio;
  }
}
