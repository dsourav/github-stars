import 'package:github_stars/core/utils/common/db_utils.dart';
import 'package:path/path.dart';

import 'package:sqflite/sqflite.dart';
import 'package:injectable/injectable.dart';
import 'package:github_stars/core/models/repository_model.dart';

@lazySingleton
class LocalDatabaseService {
  final Database _database;

  LocalDatabaseService(this._database);

  Future<List<Object?>?> insertRepositories(List<RepositoryModel> repositories) async {
    try {
      final batch = _database.batch();
      for (var repository in repositories) {
        final map = RepositoryModel.toMap(repository);
        batch.insert(
          DbUtils.repositoriesTable,
          map,
          conflictAlgorithm: ConflictAlgorithm.ignore,
        );
      }
      final data = await batch.commit();
      return data;
    } catch (_) {
      print(_.toString());
      return null;
    }
  }

  Future<List<RepositoryModel>?> getRepositories() async {
    try {
      final List<Map<String, dynamic>> maps = await _database.query(
        DbUtils.repositoriesTable,
      );

      return maps.map((map) => RepositoryModel.fromLocalDbMap(map)).toList();
    } catch (_) {
      print(_.toString());
      return null;
    }
  }

  Future<void> deleteRepositories() async {
    try {
      await _database.delete(DbUtils.dbName);
      await _database.close();
    } catch (_) {}
  }
}

@module
abstract class DatabaseModule {
  @preResolve
  @singleton
  Future<Database> get database async {
    final path = join(await getDatabasesPath(), DbUtils.dbName);
    return openDatabase(
      path,
      version: DbUtils.dbVersion,
      onCreate: (db, version) {
        db.execute(DbUtils.createRepositoriesTableQuery);
      },
    );
  }
}
