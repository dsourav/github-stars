import 'package:github_stars/core/utils/common/filter_type.dart';
import 'package:injectable/injectable.dart';

import 'package:shared_preferences/shared_preferences.dart';

@lazySingleton
class SharedPreferenceHelper {
  final SharedPreferences _sharedPreferences;
  final String _filterKey = 'filter';
  final String _lastFetchedTimeKey = "last_fetched_time";
  SharedPreferenceHelper(this._sharedPreferences);

  String getFilterType() {
    return _sharedPreferences.getString(_filterKey) ?? FilterType.star;
  }

  Future<bool> setFilterType(String value) {
    return _sharedPreferences.setString(_filterKey, value);
  }

  DateTime? getLastFetchedTime() {
    final timeInEpoch = _sharedPreferences.getInt(_lastFetchedTimeKey);
    if (timeInEpoch == null) return null;
    return DateTime.fromMillisecondsSinceEpoch(timeInEpoch, isUtc: true);
  }

  Future<bool> setLastFetchedTime(DateTime dateTime) {
    return _sharedPreferences.setInt(_lastFetchedTimeKey, dateTime.millisecondsSinceEpoch);
  }
}

@module
abstract class SharedPreferencesModule {
  @preResolve
  @lazySingleton
  Future<SharedPreferences> get sharedPreferences => SharedPreferences.getInstance();
}
