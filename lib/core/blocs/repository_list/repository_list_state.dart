part of 'repository_list_cubit.dart';

@immutable
abstract class RepositoryListState extends Equatable {
  @override
  List<Object> get props => [];
}

class RepositoryListInitial extends RepositoryListState {}

class RepositoryListLoaded extends RepositoryListState {
  final List<RepositoryModel> repositories;
  final String filterType;
  RepositoryListLoaded(this.repositories, this.filterType);

  RepositoryListLoaded copyWith({
    List<RepositoryModel>? repositories,
    String? filterType,
  }) {
    return RepositoryListLoaded(
      repositories ?? this.repositories,
      filterType ?? this.filterType,
    );
  }

  @override
  List<Object> get props => [repositories, filterType];
}

class RepositoryListLoading extends RepositoryListState {
  final List<RepositoryModel> oldRepositories;
  final bool isFirstFetch;
  RepositoryListLoading(this.oldRepositories, {this.isFirstFetch = false});

  @override
  List<Object> get props => [oldRepositories, isFirstFetch];
}

class RepositoryListFailedToLoad extends RepositoryListState {
  final String message;

  RepositoryListFailedToLoad(this.message);

  @override
  List<Object> get props => [message];
}
