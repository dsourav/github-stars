import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:github_stars/core/services/shared_preference_service.dart';
import 'package:github_stars/core/utils/common/extensions.dart';
import 'package:github_stars/core/utils/common/filter_type.dart';
import 'package:injectable/injectable.dart';

import 'package:github_stars/core/models/repository_model.dart';
import 'package:github_stars/core/services/local_database_service.dart';
import 'package:github_stars/core/repositories/github_starred/github_starred_repository.dart';

part 'repository_list_state.dart';

@injectable
class RepositoryListCubit extends Cubit<RepositoryListState> {
  final GithubStarredRepository githubStarredRepository;
  final LocalDatabaseService localDatabaseService;
  final SharedPreferenceHelper sharedPreferenceHelper;
  RepositoryListCubit(this.githubStarredRepository, this.localDatabaseService, this.sharedPreferenceHelper)
      : super(RepositoryListInitial());

  int page = 1;
  final int _pageLimit = 10;
  final String _query = "Flutter";

  getRepositoriesList({bool isScrolling = false}) async {
    if (state is RepositoryListLoading) return;

    final currentState = state;

    final sortTerm = sharedPreferenceHelper.getFilterType();
    final localRepoList = await localDatabaseService.getRepositories();
    final shouldFetchFromApi = _shouldFetchFromApi(localRepoList);
    if (shouldFetchFromApi && localRepoList?.isNotEmpty == true) localDatabaseService.deleteRepositories();

    if (shouldFetchFromApi || isScrolling) {
      var oldRepos = <RepositoryModel>[];
      if (currentState is RepositoryListLoaded) {
        oldRepos = currentState.repositories;
      }

      if (isScrolling && localRepoList?.isNotEmpty == true) {
        page = localRepoList!.length ~/ _pageLimit + 1;
      }

      emit(RepositoryListLoading(oldRepos, isFirstFetch: page == 1));
      final request = await githubStarredRepository.fetchRepositoryList(
          page: page, pageLimit: _pageLimit, query: _query, sort: sortTerm);

      request.fold((exception) => null, (result) {
        page++;
        final repos = (state as RepositoryListLoading).oldRepositories;
        repos.addAll(result);
        localDatabaseService.insertRepositories(result);
        sharedPreferenceHelper.setLastFetchedTime(DateTime.now());
        emit(RepositoryListLoaded(repos, sortTerm));
      });
    } else {
      emit(RepositoryListLoaded(localRepoList ?? [], sortTerm));
    }
  }

  bool _shouldFetchFromApi(List<RepositoryModel>? localRepoList) {
    if (localRepoList == null) return true;
    if (localRepoList.isEmpty) {
      return true;
    }

    final lastFetchedTime = sharedPreferenceHelper.getLastFetchedTime();
    if (lastFetchedTime == null) return true;
    final DateTime currentDateTime = DateTime.now();
    final Duration difference = currentDateTime.difference(lastFetchedTime);

    return difference.inMinutes > 30;
  }

  updateFilterType(String filterType) {
    final currentState = state;

    if (currentState is RepositoryListLoaded) {
      final repos = List<RepositoryModel>.from(currentState.repositories);
      if (filterType == FilterType.star) {
        repos.sort((a, b) => b.stargazersCount.compareTo(a.stargazersCount));
      } else {
        repos.sort((a, b) => b.updatedAt.toEpochTime.compareTo(a.updatedAt.toEpochTime));
      }
      emit(currentState.copyWith(repositories: repos, filterType: filterType));
      sharedPreferenceHelper.setFilterType(filterType);
    }
  }
}
