import 'package:dartz/dartz.dart';
import 'package:github_stars/core/models/repository_model.dart';
import 'package:github_stars/core/utils/exception_handler/app_exception.dart';

abstract class GithubStarredRepository {
  Future<Either<AppException, List<RepositoryModel>>> fetchRepositoryList(
      {required int page, required String query, required int pageLimit, required String sort});
}
