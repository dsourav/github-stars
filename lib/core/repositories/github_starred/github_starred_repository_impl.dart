import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:github_stars/core/utils/network_client/endpoints.dart';
import 'package:injectable/injectable.dart';

import 'package:github_stars/core/models/repository_model.dart';
import 'package:github_stars/core/repositories/github_starred/github_starred_repository.dart';
import 'package:github_stars/core/utils/exception_handler/app_exception.dart';
import 'package:github_stars/core/utils/exception_handler/dio_exception.dart';

@Injectable(as: GithubStarredRepository)
class GithubStarredRepositoryImpl implements GithubStarredRepository {
  final Dio dio;

  GithubStarredRepositoryImpl(this.dio);

  @override
  Future<Either<AppException, List<RepositoryModel>>> fetchRepositoryList(
      {required int page, required String query, required int pageLimit, required String sort}) async {
    try {
      final response =
          await dio.get("${EndPoints.searchRepositories}?q=$query&page=$page&per_page=$pageLimit&sort=$sort");
      if (response.statusCode == 200) {
        final List data = response.data['items'];
        final repositories = data.map(((item) => RepositoryModel.fromMap(item))).toList();
        return Right(repositories);
      }
      return const Right([]);
    } on DioException catch (e) {
      return Left(DioExceptionHanler(e));
    } on Exception catch (_) {
      return Left(AppException.unknown());
    }
  }
}
