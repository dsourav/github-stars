import 'package:flutter/material.dart';

import 'ui/route_handler/app_router.dart';
import 'core/utils/injectable/injectable.dart';
import 'package:github_stars/ui/style/app_theme.dart';

class GithubStarsApp extends StatelessWidget {
  const GithubStarsApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: getIt<AppRouter>().config(),
      title: 'Github Stars App',
      theme: AppTheme.buildLightTheme,
      debugShowCheckedModeBanner: false,
    );
  }
}
