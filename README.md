# github_stars

A Flutter project for fetching github stars with offline support.

## Running the App with Flutter FVM and Build Runner

This project utilizes Flutter FVM as the Flutter version management tool. Before running the app, make sure you have Flutter FVM 3.10.1 installed on your machine.

## Installing Flutter FVM

To install Flutter FVM, run the following command:

- `dart pub global activate fvm`

## Setting Up Flutter FVM

- `fvm use 3.10.1`

## Installing Dependencies

- `flutter pub get`

## Commands for generating Built Value

- `flutter packages pub run build_runner build`
- `flutter packages pub run build_runner watch`

To generate built values removing conflicts:

- `flutter packages pub run build_runner build --delete-conflicting-outputs`
